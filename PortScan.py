import sys
import socket
import argparse
from concurrent.futures import ThreadPoolExecutor

# ASCII art of a duck
duck_art = """

      ,~~.
 ,   (  - )>
 )`~~'   (
(  .__)   )
 `-.____,'   PortScanner
 
"""

print(duck_art)

# Function to probe a specific port on the target IP address
def probe_port(port):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(0.1)  # Adjust the timeout value
        r = sock.connect_ex((ip, port))
        sock.close()
        return r
    except Exception as e:
        return 1

def scan_ports(ip, ports):
    open_ports = []

    with ThreadPoolExecutor() as executor:
        # Use executor.map to run probe_port concurrently for each port
        results = executor.map(probe_port, ports)

    # Collect the results
    open_ports = [p for p, result in zip(ports, results) if result == 0]

    # Print the list of open ports or a message if no ports are open
    if open_ports:
        print("Open Ports are: ")
        print(sorted(open_ports))
    else:
        print("Looks like no ports are open :(")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Simple port scanning script with options for port range.")
    parser.add_argument("ip", help="Target IP address to scan.")
    parser.add_argument(
        "--ports",
        type=lambda s: list(map(int, s.split(","))),
        default=range(1, 1025),
        help="Specify a range of ports to scan (e.g., --ports 80,443,8080). Default is 1-1024.",
    )

    args = parser.parse_args()

    ip = args.ip
    ports = args.ports

    scan_ports(ip, ports)
