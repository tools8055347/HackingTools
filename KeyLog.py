import keyboard

# ASCII art of a duck
duck_art = """

      ,~~.
 ,   (  - )>
 )`~~'   (
(  .__)   )
 `-.____,'   HashCracker
 
"""

try:
    keys = keyboard.record(until='ENTER')
    keyboard.play(keys)
except Exception as e:
    print(f"Error: {e}")
