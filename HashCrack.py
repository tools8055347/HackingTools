import hashlib
import argparse

# ASCII art of a duck
duck_art = """

      ,~~.
 ,   (  - )>
 )`~~'   (
(  .__)   )
 `-.____,'   HashCracker
 
"""

def crack_password(wordlist_location, hash_input):
    try:
        with open(wordlist_location, 'rb') as file:
            for line in file:
                # Decode each line explicitly with UTF-8
                decoded_line = line.decode('utf-8', errors='ignore').strip()

                for algorithm in hash_algorithms:
                    hash_ob = hashlib.new(algorithm, decoded_line.encode())
                    hashed_pass = hash_ob.hexdigest()
                    if hashed_pass == hash_input:
                        print(f'Found cleartext password! {decoded_line}')
                        exit(0)
                    else:
                        print(f"Trying: {algorithm.upper()} - {decoded_line}")

        print("Password not found in the wordlist.")
    except FileNotFoundError:
        print("Error: Wordlist file not found.")
        exit(1)
    except Exception as e:
        print(f"Error: {e}")
        exit(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Simple password cracking script.")
    parser.add_argument("wordlist", help="Wordlist file location.")
    parser.add_argument("hash", help="Hash to be cracked.")
    args = parser.parse_args()

    wordlist_location = args.wordlist
    hash_input = args.hash

    hash_algorithms = ["md5", "sha256", "sha512"]  # Add more as needed

    crack_password(wordlist_location, hash_input)
